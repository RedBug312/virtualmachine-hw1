#ifndef HW1_H
#define HW1_H

#define TAKEN 1
#define NOT_TAKEN 0

struct Pile
{
	long tag;
	int count;
};

struct Quest
{
	long addr;
	int tag_count;
	int pile_size;
	struct Pile* pile;
};

struct HW1
{
	int isAssignment1;
	struct Quest quest[3];
};

void hw1_init(void);
void pile_push(struct Quest *quest, long tag);

#endif
