.PHONY: build start

DIR  := qemu
QEMU := $(DIR)/aarch64-linux-user/qemu-aarch64

build:
	cd $(DIR) && make

start:
	export TargetsOfBranch=400798
	export ExtriesOfBasicBlock=400700
	export ConditionalBranchInfo=40071c
	$(QEMU) encr-vm
